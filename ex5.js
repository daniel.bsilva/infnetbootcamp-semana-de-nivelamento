/* 05 - Escreva um programa que percorra todos os valores de um array de objetos
contendo como atributos o nome e o sexo dos alunos e permita que o usuário informe,
1 se o usuário esta presente ou 2 se o usuário faltou, e após o usuário responder
todos os itens o programa deverá exibir todos os itens da lista e o seu status de
presença. Observação: para cada item da lista respondido o valor informado pelo
usuário deverá ser armazenado para que possa estar sendo apresentado ao final do 
programa. */
const readline = require('readline-sync');

// Antes da execução 'presenca' não terá valor definido
var listaAlunos = [{ nome: 'Daniel', sexo: 'masculino', presenca: '-' },
                   { nome: 'Beatriz', sexo: 'feminino', presenca: '-' },
                   { nome: 'Jonas', sexo: 'masculino', presenca: '-' }];

console.log("Digite: \n 1 - presente\n 2- faltou");

for (var i = 0; i < listaAlunos.length; i++) {

    var presenteOuNao = readline.question(listaAlunos[i].nome + " , está presente ?");

    if (presenteOuNao === '1') {
        listaAlunos[i].presenca = "Presente";
    } else if (presenteOuNao === '2') {
        listaAlunos[i].presenca = "Faltou";
    } else {
        // Caso ele digite im valor diferente de '1' e '2'
        console.log("Valor inválido. Digite novamente!");
        // Volta 1 posição!
        i--;
    }
}
    console.log(listaAlunos);

