/* 06 - Escreva um programa solicite ao usuário o primeiro nome e o programa imprima
na tela este nome 100 vezes, porém ao imprimir o nome solicitado o programa deverá
exibir para cada linha apresentada o nome 10 vezes. Exemplo
Informe o nome: Ezer
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 1
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 2
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 3
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 4
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 5
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 6
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 7
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 8
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 9
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 10
Fim do programa */
const readline = require('readline-sync');

var nome = readline.question("Digite o nome: ");
var junta_nomes = "";

for (var i = 1; i <= 100; i++) {
    junta_nomes += nome;
    if(i % 10 == 0){
        console.log('|'+junta_nomes+'|\n');
        junta_nomes = "";
    }
}
