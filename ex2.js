/* 02 - Escreva um programa que solicite ao usuário um número qualquer e o programa
 responda imprimindo na tela se o número informado é impar ou par. */
const readline = require('readline-sync');

const num = readline.question('Número:');

if(num % 2 == 0){
    console.log('Par');
}else{
    console.log('Impar');
}