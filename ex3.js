/* 03 - Escreva um programa que solicite ao usuário um número qualquer
 e o programa responda imprimindo na tela se o número informado é um número primo. */

const readline = require('readline-sync');

const num = readline.question('Número:');

var p = 0;

//  Divisível por 1 e ele mesmo
for (var c = 1; c < num; c++) {
    for (var i = 1; i < c; i++) {
        if (num % i == 0) {
            p++;
        }
    }
}

if (p == 2) {
  console.log("O número é primo");
}else{
    console.log("O número não é primo");
}
