/* 01 - Escreva um programa que solicite ao usuário que informe o tamanho dos três lados de um triângulo e o programa retorno o tipo de triângulo correspondente aos valores informados. Observação:
    • Triângulo Equilátero 3 lados iguais
    • Triângulo Isósceles 2 lados iguais
    • Triângulo Escaleno todos os lados diferentes */
const readline = require('readline-sync');

const lado1 = readline.question('Lado 1: \n');
const lado2 = readline.question('Lado 2: \n');
const lado3 = readline.question('Lado 3: \n');

if (lado1 === lado2) {
    if (lado2 === lado3) {
        console.log('Triangulo Equilátero');
    }
}
if (lado1 !== lado2) {
    if (lado1 !== lado3) {
        if (lado2 !== lado1) {
            if (lado2 !== lado3) {
                console.log('Triangulo Escaleno');
            }
        }
    }
}