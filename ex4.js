/* 04 - Escreva um programa que apresente na tela do usuário uma lista de opções
como apresentado abaixo:

1 - Somar | 2 - Subtrair | 3 - Muiltiplicar | 4 - Dividir | -1 Sair 

Após apresentar as opções ao usuário o programa deverá recolher do usuário a opção desejada:
Se o valor informado for igual a uma operação matemática, o programa deverá solicitar o 
primeiro número ao usuário, na sequência deverá solicitar o segundo número e por fim
apresentar ao usuário o resultado final da operação. 

Se o valor informado for -1 que corresponde a opção Sair apresentada o sistema deverá
apresentar uma mensagem avisando que está desligando e o programa deve ser encerrado.

Observação: o programa deve permanecer ligado até que o usuário use a opção “-1 Sair”. */
const readline = require('readline-sync');

console.log("1 - Somar | 2 - Subtrair | 3 - Multiplicar | 4 - Dividir | -1 Sair");

const opcao = readline.question('Opção:');
// Caso a opção seja diferente de 'sair', pergunta os numeros
if (opcao != -1) {
    const num1 = parseInt(readline.question('Número 1:'));
    const num2 = parseInt(readline.question('Número 2:'));

    if (opcao == 1) {
        // chama a função 'somar'
        console.log(somar(num1, num2));
    } else if (opcao == 2) {
        // chama a função 'subtrair'
        subtrair(num1, num2);
    } else if (opcao == 3) {
        // chama a função 'multiplicar'
        multiplicar(num1, num2);
    } else if (opcao == 4) {
        // chama a função 'dividir'
        dividir(num1, num2);
    } else {
        console.log("Opção inválida!");
    }
} else {
    console.log('O programa está desligando.');
}
function somar(num1, num2) {
    return num1 + num2;
}
function subtrair(num1, num2) {
    return num1 - num2;
}
function multiplicar(num1, num2) {
    return num1 * num2;
}
function dividir(num1, num2) {
    return num1 / num2;
}


